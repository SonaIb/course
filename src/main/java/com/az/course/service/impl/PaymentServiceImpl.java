package com.az.course.service.impl;

import com.az.course.dto.*;
import com.az.course.model.Course;
import com.az.course.model.Payment;
import com.az.course.model.Student;
import com.az.course.repository.CourseRepository;
import com.az.course.repository.PaymentRepository;
import com.az.course.repository.StudentRepository;
import com.az.course.service.PaymentSevice;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentSevice {

    private final PaymentRepository paymentRepository;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final ModelMapper modelMapper;

    @Override
    public PaymentResponse createPayment(PaymentRequest paymentRequest) {
/*      Payment payment = new Payment();
        payment.setReceipt(paymentRequest.getReceipt());
        payment.setAmount(paymentRequest.getAmount());
        payment.setImage(paymentRequest.getImage());
        payment.setReceiptPaymentDate(paymentRequest.getReceiptPaymentDate());
        payment.setCoursePaymentMonth(paymentRequest.getCoursePaymentMonth());
        payment.setCardName(paymentRequest.getCardName());

        Student student = studentRepository.findById(paymentRequest.getAccountId()).orElse(null);
        payment.setStudent(student);

        Course course = lessonRepository.findById(paymentRequest.getCourseId()).orElse(null);
        payment.setCourse(course);

        payment = paymentRepository.save(payment);


        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setId(payment.getId());
        paymentResponse.setAmount(payment.getAmount());
        paymentResponse.setCoursePaymentMonth(payment.getCoursePaymentMonth());
        paymentResponse.setReceipt(payment.getReceipt());
        paymentResponse.setReceiptPaymentDate(payment.getReceiptPaymentDate());
        paymentResponse.setCardName(payment.getCardName());
        paymentResponse.setImage(payment.getImage());


        StudentResponse studentResponse = new StudentResponse();
        studentResponse.setId(student.getId());
        studentResponse.setName(student.getName());

        CourseResponse courseResponse = new CourseResponse();
        courseResponse.setId(course.getId());
        courseResponse.setName(course.getName());

        paymentResponse.setStudent(studentResponse);
        paymentResponse.setCourse(courseResponse);

        return paymentResponse;*/

        Payment payment = modelMapper.map(paymentRequest, Payment.class);
        Student student = studentRepository.findById(paymentRequest.getStudentId()).orElse(null);
        payment.setStudent(student);

        Course course = courseRepository.findById(paymentRequest.getCourseId()).orElse(null);
        payment.setCourse(course);
        payment = paymentRepository.save(payment);

        PaymentResponse paymentResponse = modelMapper.map(payment, PaymentResponse.class);

        return paymentResponse;
    }

    @Override
    public List<PaymentResponse> getAllPayment() throws Exception {
        List<Payment> payments = paymentRepository.findAll();

        List<PaymentResponse> paymentResponses = new ArrayList<>();
        for (Payment payment : payments) {
            PaymentResponse paymentResponse = modelMapper.map(payment, PaymentResponse.class);
            Student student = payment.getStudent();
            if (student != null) {
                StudentResponse studentResponse = modelMapper.map(student, StudentResponse.class);
                paymentResponse.setStudent(studentResponse);
            }
            Course course = payment.getCourse();
            if (course != null) {
                CourseResponse courseResponse = modelMapper.map(course, CourseResponse.class);
                paymentResponse.setCourse(courseResponse);
            }

            paymentResponses.add(paymentResponse);
        }

        return paymentResponses;
    }

    @Override
    public PaymentResponse updatePayment(Long paymentId, PaymentRequest paymentRequest) {
        Payment payment = paymentRepository.findById(paymentId).orElse(null);

        if (payment != null) {
            payment.setReceipt(paymentRequest.getReceipt());
            payment.setAmount(paymentRequest.getAmount());
            payment.setImage(paymentRequest.getImage());
            payment.setReceiptPaymentDate(paymentRequest.getReceiptPaymentDate());
            payment.setCoursePaymentMonth(paymentRequest.getCoursePaymentMonth());
            payment.setCardName(paymentRequest.getCardName());


            Student student = studentRepository.findById(paymentRequest.getStudentId()).orElse(null);
            payment.setStudent(student);

            Course course = courseRepository.findById(paymentRequest.getCourseId()).orElse(null);
            payment.setCourse(course);

            payment = paymentRepository.save(payment);

            PaymentResponse paymentResponse = new PaymentResponse();
            paymentResponse.setId(payment.getId());
            paymentResponse.setAmount(payment.getAmount());
            paymentResponse.setCoursePaymentMonth(payment.getCoursePaymentMonth());
            paymentResponse.setReceipt(payment.getReceipt());
            paymentResponse.setReceiptPaymentDate(payment.getReceiptPaymentDate());
            paymentResponse.setCardName(payment.getCardName());
            paymentResponse.setImage(payment.getImage());

            if (student != null) {
                StudentResponse studentResponse = new StudentResponse();
                studentResponse.setId(student.getId());
                studentResponse.setName(student.getName());
                paymentResponse.setStudent(studentResponse);
            }

            if (course != null) {
                CourseResponse courseResponse = new CourseResponse();
                courseResponse.setId(course.getId());
                courseResponse.setName(course.getName());
                paymentResponse.setCourse(courseResponse);
            }

            return paymentResponse;
        } else {
            throw new EntityNotFoundException("Payment with ID " + paymentId + " not found.");
        }
    }
    @Override
    public List<PaymentResponse> filterPaymentsByDateRange(Date from, Date to) {
        List<Payment> payments = paymentRepository.findByReceiptPaymentDateBetween(from, to);

        List<PaymentResponse> paymentResponses = new ArrayList<>();
        for (Payment payment : payments) {
            PaymentResponse paymentResponse = modelMapper.map(payment, PaymentResponse.class);
            paymentResponses.add(paymentResponse);
        }
        return paymentResponses;
    }


}
