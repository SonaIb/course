package com.az.course.service.impl;

import com.az.course.dto.*;
import com.az.course.model.Course;
import com.az.course.model.Student;
import com.az.course.repository.CourseRepository;
import com.az.course.repository.StudentRepository;
import com.az.course.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public CourseResponse create(CourseRequest courseRequest) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        Course course = modelMapper.map(courseRequest, Course.class);
        courseRepository.save(course);

        // After saving the course, you can create a CourseResponse object and return it
        CourseResponse courseResponse = modelMapper.map(course, CourseResponse.class);
        return courseResponse;
    }

    public CourseResponseWithPhone addCourseToStudent(Long courseId, Long studentId) {
        Course course = courseRepository.findById(courseId).orElse(null);
        Student student = studentRepository.findById(studentId).orElse(null);

        if (course != null && student != null) {
            student.getCourses().add(course);
            studentRepository.save(student);
        }

        return createCourseResponseWithPhone(course, student);
    }

    @Override
    public CourseResponse updateCourse(Long courseId, CourseRequest courseRequest) {
        Course course = courseRepository.findById(courseId).orElse(null);

        if (course != null) {
            course.setName(courseRequest.getName());


            course = courseRepository.save(course);

            CourseResponse courseResponse = new CourseResponse();
            courseResponse.setId(course.getId());
            courseResponse.setName(course.getName());


            return courseResponse;
        }else {

        throw new EntityNotFoundException("Payment with ID " + courseId + " not found.");
    }


}

    @Override
    public List<CourseResponseList> getAllCourses() {
        List<CourseResponseList> courseResponses = new ArrayList<>();

        List<Course> courses = courseRepository.findAll();

        for (Course course : courses) {
            CourseResponseList courseResponse = new CourseResponseList();
            courseResponse.setId(course.getId());
            courseResponse.setName(course.getName());

            List<Student> students = course.getStudents();
            List<StudentResponsePhone> studentResponsePhones = new ArrayList<>();

            for (Student student : students) {
                StudentResponsePhone studentResponsePhone = new StudentResponsePhone();
                studentResponsePhone.setId(student.getId());
                studentResponsePhone.setPhoneNumber(student.getPhoneNumber());

                studentResponsePhones.add(studentResponsePhone);
            }

            courseResponse.setStudentResponsePhone(studentResponsePhones);
            courseResponses.add(courseResponse);
        }

        return courseResponses;
    }

    private CourseResponseWithPhone createCourseResponseWithPhone(Course course, Student student) {
        CourseResponseWithPhone response = new CourseResponseWithPhone();
        response.setId(course.getId());
        response.setName(course.getName());

        StudentResponsePhone studentResponsePhone = new StudentResponsePhone();
        studentResponsePhone.setId(student.getId());
        studentResponsePhone.setPhoneNumber(student.getPhoneNumber());

        response.setStudentResponsePhone(studentResponsePhone);

        return response;
    }

    @Override
    public CourseResponseList getCoursesByName(String courseName) {
        CourseResponseList courseResponse = new CourseResponseList();
        Course course = courseRepository.findByName(courseName);

        if (course != null) {
            courseResponse.setId(course.getId());
            courseResponse.setName(course.getName());

            List<Student> students = course.getStudents();
            List<StudentResponsePhone> studentResponsePhones = new ArrayList<>();

            for (Student student : students) {
                StudentResponsePhone studentResponsePhone = new StudentResponsePhone();
                studentResponsePhone.setId(student.getId());
                studentResponsePhone.setPhoneNumber(student.getPhoneNumber());

                studentResponsePhones.add(studentResponsePhone);
            }

            courseResponse.setStudentResponsePhone(studentResponsePhones);
        }

        return courseResponse;
    }
}
