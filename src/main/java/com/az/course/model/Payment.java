package com.az.course.model;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "payment")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Double  amount;
    String  coursePaymentMonth;
    String receipt;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    Date receiptPaymentDate;
    @ManyToOne
    @JoinColumn(name = "course_id")
    Course course;
    @ManyToOne
    @JoinColumn(name = "student_id")
    Student student;
    String image;
    private String cardName;

}
