package com.az.course.service;

import com.az.course.dto.*;

import java.util.List;

public interface CourseService {

    CourseResponse create(CourseRequest courseRequest);
    CourseResponseWithPhone addCourseToStudent(Long courseId, Long studentId);

    CourseResponse updateCourse(Long courseId, CourseRequest courseRequest);

    List<CourseResponseList> getAllCourses();

    CourseResponseList getCoursesByName(String courseName);

}
