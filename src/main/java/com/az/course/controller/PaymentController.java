package com.az.course.controller;

import com.az.course.dto.PaymentRequest;
import com.az.course.dto.PaymentResponse;
import com.az.course.service.impl.PaymentServiceImpl;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/payment")
@RequiredArgsConstructor
@SecurityRequirement(name="jwtAuth")
public class PaymentController {
    private final PaymentServiceImpl paymentService;

    @PostMapping()
    public ResponseEntity<PaymentResponse> createPayment(@RequestBody PaymentRequest payment) {
        PaymentResponse createdPayment = paymentService.createPayment(payment);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdPayment);
    }

    @GetMapping("/all")
    public ResponseEntity<List<PaymentResponse>> getAllPayment() throws Exception {
        List<PaymentResponse> paymentList = paymentService.getAllPayment();
        return ResponseEntity.ok(paymentList);
    }

    @PutMapping("/{paymentId}")
    public ResponseEntity<PaymentResponse> updatePayment(@PathVariable Long paymentId, @RequestBody PaymentRequest paymentRequest) throws Exception {
        PaymentResponse updatedPayment = paymentService.updatePayment(paymentId, paymentRequest);
        return ResponseEntity.ok(updatedPayment);
    }

    @GetMapping("/filter")
    public ResponseEntity<List<PaymentResponse>> filterPayments(
            @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date from,
            @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date to) {
        List<PaymentResponse> payments = paymentService.filterPaymentsByDateRange(from, to);
        return ResponseEntity.ok(payments);
    }

}
