package com.az.course.repository;

import com.az.course.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findStudentByPhoneNumber(String phoneNumber);
}
