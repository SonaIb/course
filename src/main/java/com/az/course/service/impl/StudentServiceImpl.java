package com.az.course.service.impl;

import com.az.course.dto.StudentDto;
import com.az.course.model.Student;
import com.az.course.repository.StudentRepository;
import com.az.course.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public void create(StudentDto dto) {
        Student student = modelMapper.map(dto, Student.class);
        studentRepository.save(student);
    }

    @Override
    public void update(Long studentId, StudentDto studentDto) {
        Student existingStudent = studentRepository.findById(studentId).orElse(null);
        if (existingStudent != null) {
            modelMapper.map(studentDto, existingStudent);

            studentRepository.save(existingStudent);

            System.out.println("Student with ID " + studentId + " has been updated.");
        } else {
            System.out.println("Student with ID " + studentId + " not found.");
        }
    }

    @Override
    public StudentDto findStudentByPhoneNumber(String phoneNumber) {
        Student student = studentRepository.findStudentByPhoneNumber(phoneNumber);
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public List<StudentDto> getAllStudent(){
        List<Student> students = studentRepository.findAll();
        return students.stream()
                .map(student -> modelMapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
    }
}
