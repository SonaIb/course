package com.az.course.dto;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CourseResponse {

    Long id;
    String name;
}
