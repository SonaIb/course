package com.az.course.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentResponse {
    Long id;
    String receipt;
    double amount;
    StudentResponse student;
    CourseResponse course;
    Date receiptPaymentDate;
    String coursePaymentMonth;
    String cardName;
    String image;
}
