package com.az.course.service;

import com.az.course.dto.StudentDto;
import com.az.course.model.Payment;

import java.util.List;


public interface StudentService {
    void create(StudentDto dto);
    void update(Long studentId, StudentDto studentDto);
    StudentDto findStudentByPhoneNumber(String phoneNumber);
    List<StudentDto> getAllStudent() throws Exception;

}
