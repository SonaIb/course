package com.az.course.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentRequest {

     Long id;
     String receipt;
     double amount;
     Long studentId;
     Long courseId;
     Date receiptPaymentDate;
     String coursePaymentMonth;
     String cardName;
     String image;
}
