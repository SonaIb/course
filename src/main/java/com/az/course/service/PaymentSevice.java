package com.az.course.service;

import com.az.course.dto.PaymentRequest;
import com.az.course.dto.PaymentResponse;
import java.util.Date;
import java.util.List;

public interface PaymentSevice {

    public PaymentResponse createPayment(PaymentRequest paymentRequest);

    List<PaymentResponse> getAllPayment() throws Exception;

    PaymentResponse updatePayment(Long paymentId, PaymentRequest paymentRequest);

    List<PaymentResponse> filterPaymentsByDateRange(Date from, Date to);
}
