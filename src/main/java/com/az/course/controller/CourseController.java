package com.az.course.controller;

import com.az.course.dto.CourseRequest;
import com.az.course.dto.CourseResponse;
import com.az.course.dto.CourseResponseList;
import com.az.course.dto.CourseResponseWithPhone;
import com.az.course.service.impl.CourseServiceImpl;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
@SecurityRequirement(name="jwtAuth")
public class CourseController {

    private final CourseServiceImpl courseService;

    @PostMapping
    public ResponseEntity<CourseResponse> create(@RequestBody CourseRequest courseRequest){
        CourseResponse courseResponse = courseService.create(courseRequest);
        return ResponseEntity.ok(courseResponse);
    }

    @PostMapping("/addCourseToStudent")
    public ResponseEntity<CourseResponseWithPhone> addCourseToStudent(
            @RequestParam Long courseId,
            @RequestParam Long studentId) {

        CourseResponseWithPhone response = courseService.addCourseToStudent(courseId, studentId);

        if (response != null) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<CourseResponse> updateCourse(@PathVariable Long id, @RequestBody CourseRequest courseRequest) {
        CourseResponse course = courseService.updateCourse(id, courseRequest);
        if (course != null) {
            return ResponseEntity.ok(course);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/all")
    public List<CourseResponseList> getAllCourses() {
        return courseService.getAllCourses();
    }

    @GetMapping("/byName")
    public CourseResponseList getCoursesByName(@RequestParam String courseName) {
        return courseService.getCoursesByName(courseName);
    }
}
