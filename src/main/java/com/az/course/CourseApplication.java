package com.az.course;

import com.az.course.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.az.course.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@RequiredArgsConstructor
public class CourseApplication {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;

	@PostConstruct
	public void createUsers() {
		List<User> user = Stream.of(
				new User(1L, "user" , passwordEncoder.encode("password")),
				new User(2L, "Admin" , passwordEncoder.encode("@Ingress12"))
		).collect(Collectors.toList());
		userRepository.saveAll(user);
	}
	public static void main(String[] args) {
		SpringApplication.run(CourseApplication.class, args);
	}

}
