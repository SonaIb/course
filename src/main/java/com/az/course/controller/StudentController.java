package com.az.course.controller;
import com.az.course.dto.StudentDto;
import com.az.course.service.impl.StudentServiceImpl;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
@SecurityRequirement(name="jwtAuth")
public class StudentController {
    private final StudentServiceImpl studentService;

    @PostMapping
    public void create(@RequestBody StudentDto studentDto){
        studentService.create(studentDto);
    }

    @PutMapping("/{studentId}")
    public ResponseEntity<String> updateStudent(@PathVariable Long studentId, @RequestBody StudentDto studentDto) {
        studentService.update(studentId, studentDto);
        return ResponseEntity.ok("Student updated successfully");
    }

    @GetMapping("/search")
    public StudentDto searchByTelephoneNumber(@RequestParam String phoneNumber) {
        return studentService.findStudentByPhoneNumber(phoneNumber);
    }

    @GetMapping("/all")
    public ResponseEntity<List<StudentDto>> getAllStudents() {
        List<StudentDto> studentList = studentService.getAllStudent();
        return ResponseEntity.ok(studentList);
    }
}
