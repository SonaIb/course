package com.az.course.service;

import com.az.course.model.Expense;

import java.util.List;

public interface ExpenseService {

    Expense create(Expense expense);
    Expense updateExpense(Long id, Expense updatedExpense);
    List<Expense> getAllExpenses();
}