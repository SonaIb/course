package com.az.course.service.impl;

import com.az.course.model.Expense;
import com.az.course.repository.ExpenseRepository;
import com.az.course.service.ExpenseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ExpenseServiceImpl implements ExpenseService {

    private final ExpenseRepository expenseRepository;
    @Override
    public Expense create(Expense expense) {
       return  expenseRepository.save(expense);
    }

    @Override
    public Expense updateExpense(Long id, Expense updatedExpense) {
        Expense existingExpense = expenseRepository.findById(id).orElse(null);
        if (existingExpense != null) {
            existingExpense.setName(updatedExpense.getName());
            existingExpense.setAmount(updatedExpense.getAmount());
            existingExpense.setDescription(updatedExpense.getDescription());
            existingExpense.setReceipt(updatedExpense.getReceipt());
            existingExpense.setReceiptPaymentDate(updatedExpense.getReceiptPaymentDate());
            existingExpense.setImage(updatedExpense.getImage());
            return expenseRepository.save(existingExpense);
        }
        return null;
    }

    @Override
    public List<Expense> getAllExpenses() {
        return expenseRepository.findAll();
    }
}
